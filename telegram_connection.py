#This file was created by the TweeToRasa Compiler of the Gitlab Group StoryBot
"""
Creates a connection to Telegram through the Python Telegram Bot API (https://python-telegram-bot.readthedocs.io/en/stable/) which needs to be installed like this (https://github.com/python-telegram-bot/python-telegram-bot#installing).

Should now be able to handle multiple users and multiple answers.
Should now be able to recognize another user's message while first user
is waiting for a delayed message.
Should now be able to decide on the length of the "is typing" with a simple rule and start
displaying it only shortly before delay is over if delay is long.
Should now use the defined delay time from the "delays.txt" file.
Should now ignore incoming messages from user who is currently in delay and reply with the specified default answer.
"""

import os

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

import time
from threading import Timer

import rasa_core
from rasa_core.agent import Agent
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.policies.memoization import MemoizationPolicy
from rasa_core.interpreter import RasaNLUInterpreter
from rasa_core.utils import EndpointConfig
from rasa_core import config
from rasa_core.channels import UserMessage

# define global agent
agent = ""

# dictionary with structure:
# { user_id : is_delay_active }
user_state = {}

# dictionary with structure: 
# { passage_name : delay_time in seconds }
delay_times = {}

# answer the user gets when writing to the bot while delay is active
default_delay_answer = "Sorry, I'm busy at the moment."

# waits for seconds (currently not used)
def delay(seconds):
	time.sleep(seconds)

# tell telegram to display "is typing" for duration (max = 5s)
def set_typing(bot, id, duration=5.0):
	bot.send_chat_action(id, "typing", timeout=duration)
	
# sends the text message over the update object
def utter_delayed(update, text):
	global user_state
	user_state[update.message.from_user.id] = False
	update.message.reply_text(text)

# starts the rasa bot accessible by global agent
def start_rasa_bot():
	interpreter = RasaNLUInterpreter('./models/nlu/default/chat')
	action_endpoint = EndpointConfig(url="http://localhost:5055/webhook")
	global agent
	agent = Agent.load('./models/dialogue', interpreter=interpreter, action_endpoint=action_endpoint)

# react to a users message
def rasa_answer(bot, update):
	# get users message
	message = update.message
	user_id = message.from_user.id
	print(str(user_id) + " says: " + message.text)
	
	# check if user is in delay
	global user_state
	
	if user_id in user_state and user_state[user_id] == True:
		# reply with default answer
		global default_delay_answer
		update.message.reply_text(default_delay_answer)
		return
	
	# get bots answers
	global agent
	user_message = UserMessage(message.text, None, str(user_id))
	answers = agent.handle_message(user_message)
	tracker = agent.log_message(user_message)
	
	# find the delay_time for the passages
	latest_passage_names = get_latest_passage_names(tracker, len(answers))
	
	global delay_times
	
	# store the duration of the sums of the delay before each answer
	delay_sum = 0.0
	
	# send all of bots answers
	for i in range(0, len(answers)):
	
		answer = answers[i]
		print("Bot says: " + answer['text'])
		
		# find delay time for this answer
		passage_name = latest_passage_names[i]
		delay_time = 1.0 #default
		
		if passage_name in delay_times:
			delay_time = float(delay_times[passage_name])
			
		# add sum of previous delays to preserve order of answers
		# save old sum for below
		old_sum = delay_sum
		delay_time = delay_sum + delay_time
		delay_sum = delay_time
		
		# decide duration of "is typing" according to message length
		text_len = len(answer['text'])
		typing_duration = float(text_len) / 10.0
		
		if typing_duration < 2.0:
			typing_duration = 2.0
		elif typing_duration > 5.0:
			typing_duration = 5.0
		
		# start a timer running parallel to the main thread and calling utter_delayed()
		t = Timer(delay_time, utter_delayed, [update, answer['text']])
		t.start()
		
		# update state of user
		user_state[user_id] = True
		
		# typing duration must not be longer than time between sending 
		# previous answer and this one
		max_typing_duration = delay_time - old_sum - 0.1
		
		if typing_duration >= max_typing_duration:
			typing_duration = max_typing_duration
		
		# if delay is too long wait until shortly before end to set "is typing"
		if delay_time > typing_duration:
			typing_delay = delay_time - typing_duration
			t = Timer(typing_delay, set_typing, [bot, message.chat.id, typing_duration])
			t.start()
		else:
			set_typing(bot, message.chat.id, typing_duration)
			

def main():
	# start the rasa bot instance
	start_rasa_bot()
	
	# configure telegram bot connection
	updater = Updater(token='731134851:AAEvE1OLl1HVka1x2HSi_cRXY5gEBUBRYJA')
	dispatcher = updater.dispatcher
	
	print("--- BOT STARTED ---")
	
	# let rasa handle the messages
	rasa_answer_handler = MessageHandler(Filters.text, rasa_answer)
	dispatcher.add_handler(rasa_answer_handler)
	
	# start listening to telegram bot
	updater.start_polling()
	
	# wait till ended
	updater.idle()
	
	print("--- BOT ENDED ---")
	

# finds the names of the last number of passages in order of appearance
def get_latest_passage_names(tracker, number):
	global agent
	
	# get events
	latest_events = tracker.events
	latest_passage_names = []
	
	# find name of the latest utter action (contains name of passage)
	for event in reversed(latest_events):
		# if all are found, stop
		if number <= 0:
			break
		if event.type_name == "action" and event.action_name.startswith("utter"):
			utter_name = event.action_name
			latest_passage_names.append(utter_name[6:])
			number -= 1
	
	# get the order right
	latest_passage_names.reverse()
	
	return latest_passage_names
	
	
# parse the delays.txt into the delays dictionary
def parse_delays_file():
	
	# read the file
	if not os.path.exists("delays.txt"):
		print("No delay definition found -> using default")
		return
	
	delays_file = open("delays.txt", "r", encoding="utf-8")
	file_lines = delays_file.readlines()
	
	if len(file_lines) == 0:
		return
		
	# default answer is always in first line
	global default_delay_answer
	global delay_times
	default_delay_answer = file_lines[0]
	
	# for all following lines
	for i in range(1, len(file_lines)):
		line = file_lines[i]
		halfs = line.split(" ")
		
		passage_name = halfs[0]
		time = int(halfs[1].strip())
		
		delay_times[passage_name] = time
	
	
if __name__ == '__main__':
	parse_delays_file()
	main()