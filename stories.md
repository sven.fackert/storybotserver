## story0
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* Ja
	 -utter_DatenBestätigen
* Ja
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story1
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* Ja
	 -utter_DatenBestätigen
* Ja
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story2
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* Ja
	 -utter_DatenBestätigen
* Ja
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story3
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* Ja
	 -utter_DatenBestätigen
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story4
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* Ja
	 -utter_DatenBestätigen
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story5
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* Ja
	 -utter_DatenBestätigen
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story6
* start
> check_utter_start

## story7
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* Nein
> check_utter_start

## story8
* start
> check_utter_start

## story9
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Ja
> check_utter_start

## story10
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story11
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story12
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story13
* start
> check_utter_start

## story14
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Ja
> check_utter_start

## story15
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story16
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story17
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* GeheimzahlGeben
	 -utter_GeheimzahlGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story18
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story19
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story20
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story21
* start
> check_utter_start

## story22
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Ja
> check_utter_start

## story23
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story24
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story25
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story26
* start
> check_utter_start

## story27
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Ja
> check_utter_start

## story28
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story29
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story30
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* KreditGeben
	 -utter_KreditGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story31
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story32
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story33
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story34
* start
> check_utter_start

## story35
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Ja
> check_utter_start

## story36
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story37
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story38
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story39
* start
> check_utter_start

## story40
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Ja
> check_utter_start

## story41
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story42
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story43
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* GeburtsdatumGeben{"Geburt": "22.10.1990"}
	 -utter_GeburtsdatumGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story44
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story45
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story46
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story47
* start
> check_utter_start

## story48
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Ja
> check_utter_start

## story49
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story50
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story51
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story52
* start
> check_utter_start

## story53
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Ja
> check_utter_start

## story54
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story55
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story56
* start
> check_utter_start
	 -utter_start
* NameGeben{"SpielerName": "hans"}
	 -action_set_nameGegeben_NameGeben
	 -utter_NameGeben
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story57
* start
> check_utter_start
	 -utter_start
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story58
* start
> check_utter_start
	 -utter_start
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story59
* start
> check_utter_start
	 -utter_start
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story60
* start
> check_utter_start

## story61
> check_utter_start
	 -utter_start
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Ja
> check_utter_start

## story62
* start
> check_utter_start
	 -utter_start
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story63
* start
> check_utter_start
	 -utter_start
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story64
* start
> check_utter_start
	 -utter_start
* ProduktFragen
	 -utter_ProduktFragen
* Ja
	 -utter_JaGekauft
* ProduktSagen{"Produkt": "pfannenset"}
	 -utter_ProduktSagen
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story65
* start
> check_utter_start

## story66
> check_utter_start
	 -utter_start
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Ja
> check_utter_start

## story67
* start
> check_utter_start
	 -utter_start
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Ja
	 -utter_ÜberwältigungBestätigen
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story68
* start
> check_utter_start
	 -utter_start
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* Nein
	 -utter_ÜberwältigungAbstreiten
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

## story69
* start
> check_utter_start
	 -utter_start
* ProduktFragen
	 -utter_ProduktFragen
* Nein
	 -utter_NeinGekauft
* Nein
	 -utter_Verweigern
* SingularitätFragen
	 -utter_SingularitätFragen
	 -action_cond_SingularitätFragen_96
	 -utter_Hilfe
* URLAngeben{"URL": "bild.de"}
	 -action_cond_URLAngeben_111

