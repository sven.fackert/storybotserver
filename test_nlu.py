from rasa_nlu.model import Interpreter

# loading the interpreter 
interpreter = Interpreter.load('./models/nlu/default/chat')

# define function to ask question
def ask_question(text):
    print(interpreter.parse(text))

# asking question
#ask_question("Hey")
#ask_question("I am great")
ask_question("I am Kai")
