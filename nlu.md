## intent:NameGeben
- ich heiße [hans] (SpielerName)
- mein name ist [bob] (SpielerName)
- [cordula] (SpielerName)
- ich bin [anna] (SpielerName)

## intent:Nein
- nein
- hab ich nicht
- stimmt nicht
- nein ich bin nicht überwältigt
- ist mir egal
- nein gebe ich dir nicht
- will ich nicht sagen
- nein die daten sind falsch
- ich will dir mein Geburtsdatum nicht sagen
- nein die Daten sind falsch

## intent:ProduktFragen
- produkt
- was für ein produkt?
- um welches produkt geht es?
- welches produkt?
- für welches produkt?

## intent:GeburtsdatumGeben
- [22.10.1990] (Geburt)
- [5. april 78] (Geburt)
- [02.04.] (Geburt)
- [6.12.82] (Geburt)
- [2. märz] (Geburt)

## intent:Ja
- ja
- hab ich
- stimmt
- richtig
- ich bin überwältigt
- das ist wahnsinn
- erstaunlich
- ja das stimmt

## intent:SingularitätFragen
- singularität
- was ist eigentlich eine singularität?
- ich weiß nicht, was einen singularität ist
- erkäre singularität

## intent:GeheimzahlGeben
- 993
- 231
- 478
- 266
- 551
- 129

## intent:KreditGeben
- 344585350736596
- 5324505194693434
- 5510298414726390
- 4669186156707763

## intent:ProduktSagen
- ein [pfannenset] (Produkt)
- ein neues [telefon] (Produkt)
- ich habe einen [Fernseher] (Produkt) gekauft
- [E-Reader] (Produkt)

## intent:URLAngeben
- www.[bild.de] (URL)
- [google.com] (URL)
- http://www.[bundesregierung.de] (URL)
- die adresse ist [katzen.de] (URL)
- sie lautet [wikipedia.org] (URL)

## intent:start
- start
- new game
- start game
- start story
- hi
- hey
- hi, start the story
- start story
- START
- Start
- Story

