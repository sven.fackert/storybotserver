from rasa_core.interpreter import RasaNLUInterpreter
from rasa_core.agent import Agent

# Loading the Agent
rasaNLU = RasaNLUInterpreter("models/nlu/default/chat")
agent = Agent.load("models/dialogue", interpreter= rasaNLU)

print(agent.handle_message('my name is Kai'))
