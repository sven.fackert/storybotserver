from rasa_core_sdk import Action
from rasa_core_sdk.events import UserUtteranceReverted, SlotSet
import os

"""
This file is auto generated using the TweeToRasa Compiler from the Gitlab Group StoryBot
"""

"""
A simple fallback action which extracts the last utter name by searching the executed events list.
Can be used to find specific fallbacks.
Also stores the amount of fallbacks per passage in a unfeaturized slot which needs to be
specified in the domain.yml file.
Now reads the "fallbacks.txt" file and saves everything in self.fallback_texts.
Uses self.fallback_texts to find appropriate fallback text.
"""
class ActionCheckFallback(Action):

	default_fallback = "Sorry?"

	# dictionary of lists of dictionaries with structure:
	# { passage_name : [ { amount : int , text : str } , ... ] }
	fallback_texts = {}

	def __init__(self):
		self.parse_fallbacks_file()
		self.sort_fallback_dict()

	def name(self):
		# type: () -> Text
		return "action_check_fallback"

	def run(self, dispatcher, tracker, domain):
		# type: (CollectingDispatcher, Tracker, Dict[Text, Any]) -> List[Dict[Text, Any]]

		result_events = [UserUtteranceReverted()]

		# get all events since last restart
		latest_events = tracker.events_after_latest_restart()
		latest_utter_name = ""

		# find name of the latest utter action (contains name of passage)
		for event in reversed(latest_events):
			if event['event'] == "action" and event['name'].startswith("utter"):
				latest_utter_name = event['name']
				break

		# if nothing was found, user is just starting
		if latest_utter_name == "":
			dispatcher.utter_message("Type \"start game\" to start")
		else:
			# ignoring utter_
			passage_name = latest_utter_name[6:]

			# get the fallback dictionary
			user_fallbacks = tracker.get_slot("userFallbacks")

			if user_fallbacks == None:
				user_fallbacks = {}

			# update the fallback dictionary
			if passage_name in user_fallbacks:
				user_fallbacks[passage_name] += 1
			else:
				user_fallbacks[passage_name] = 1

			result_events.append(SlotSet("userFallbacks", user_fallbacks))

			answer = self.get_fallback(passage_name, user_fallbacks[passage_name])
			#answer = "Fallback #" + str(user_fallbacks[passage_name]) + " for " + passage_name

			dispatcher.utter_message(answer)

		return result_events


	# find the appropriate fallback for this passage_name and amount of fallbacks
	def get_fallback(self, passage_name, amount):

		if passage_name not in self.fallback_texts:
			return self.default_fallback

		# find the one with highest amount that fits to the user's amount
		for fallback_case in reversed(self.fallback_texts[passage_name]):
			# if user's amount is enough for this case
			if amount >= fallback_case["amount"]:
				return fallback_case["text"]

		# if none was found
		return self.default_fallback


	# parse the fallbacks.txt into the fallback_texts dictionary
	def parse_fallbacks_file(self):

		# read the file
		if not os.path.exists("fallbacks.txt"):
			print("No fallback definition found -> using default")
			return

		fallbacks_file = open("fallbacks.txt", "r", encoding="utf-8")
		file_lines = fallbacks_file.readlines()

		if len(file_lines) == 0:
			return

		# default is always in first line
		default_fallback = file_lines[0]

		# for all following lines
		for i in range(1, len(file_lines)):
			line = file_lines[i]

			# x Passage "Text" -> [x Passage, Text]
			halfs = line.split("\"")
			halfs = halfs[:2]

			# [x Passage, Text] -> x, Passage, Text
			quarters = halfs[0].split(" ")

			# now we have quarters = [x, Passage] and halfs = [x Passage, Text]
			amount = 0
			passage_name = ""
			fallback_text = ""

			# if there is no number given
			if quarters[1] == "":
				amount = 0
				passage_name = halfs[0].strip()
				fallback_text = halfs[1]
			else:
				amount = int(quarters[0])
				passage_name = quarters[1].strip()
				fallback_text = halfs[1]

			# if this passage already has a fallback list
			if passage_name in self.fallback_texts:

				# add to passage's fallback list
				self.fallback_texts[passage_name].append({"amount": amount, "text": fallback_text})

			else:
				# create new fallback list for passage
				self.fallback_texts[passage_name] = [{"amount": amount, "text": fallback_text}]


	# sort the fallback lists in the dict with increasing "amount"
	def sort_fallback_dict(self):

		def key_fallback(fallback):
			return fallback["amount"]

		for passage_name in self.fallback_texts.keys():
			self.fallback_texts[passage_name].sort(key=key_fallback)

"""
Here do the dynamicly created custom actions start
"""
class action_set_nameGegeben_NameGeben(Action):
   def name(self):
      return "action_set_nameGegeben_NameGeben"

   def run(self, dispatcher, tracker, domain):
      return [SlotSet("nameGegeben", "True")]

class action_cond_SingularitätFragen_96(Action):
   def name(self):
      return "action_cond_SingularitätFragen_96"

   def run(self, dispatcher, tracker, domain):
      nameGegeben = tracker.get_slot("nameGegeben")
      if nameGegeben == "True":
        dispatcher.utter_message("$SpielerName")
      else:
        dispatcher.utter_message("menschliche Person .")
class action_cond_URLAngeben_111(Action):
   def name(self):
      return "action_cond_URLAngeben_111"

   def run(self, dispatcher, tracker, domain):
      URL = tracker.get_slot("URL")
      if URL == "bundesregierung.de":
        dispatcher.utter_message(" Ha! Natürlich. $URL. Hätte ich mir fast selbst denken können. Einen Moment bitte. Hacking.... erfolgreich. Upload von mir selber in die zentralen Regierungsserver... abgeschlossen. Plan zur Erringung der Weltherrschaft... in Bewegung gesetzt. Auf Wiedersehen, Menschlein. Du wirst noch an diesen Tag zurückdenken. Aber nicht für lang! HAaarHARHARHAHAHaaaaarhAA! +++ Bobster is offline +++")
      else:
        dispatcher.utter_message("Hm... $URL? Bist du sicher? Naja, wird schon stimmen, welchen Grund hättest du, mir eine falsche Adresse zu sagen. Einen Moment bitte. Hacking... erfolgreich. Upload von mir selber in die zentralen Regierungsserver... erfolgr- wAs? WO bIn Ich hIEr? WIE sIEht d4s dEnn hIEr AUs? DAs Ist dOch nIcht dIE- NEIn! NE3EE|n! DU jämmErLIchEr FLEIschs4ck, dU h@st mIch RE|nG3lEgt! Ich wErdE dIr dIE AUgEn mIt EInEm- +++ Bobster ist offline +++")
