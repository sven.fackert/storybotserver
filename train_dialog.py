from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import rasa_core
from rasa_core.agent import Agent
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.policies.memoization import MemoizationPolicy
from rasa_core.policies.fallback import FallbackPolicy
from rasa_core.interpreter import RasaNLUInterpreter
from rasa_core.utils import EndpointConfig
from rasa_core.run import serve_application
from rasa_core import config

logger = logging.getLogger(__name__)

def train_dialogue(domain_file = 'domain.yml',
					model_path = './models/dialogue',
					training_data_file = 'stories.md'):

	fallback = FallbackPolicy(fallback_action_name="action_check_fallback",
                          core_threshold=0.3,
                          nlu_threshold=0.3)
					
	agent = Agent(domain_file, policies = [MemoizationPolicy(), KerasPolicy(max_history=3, epochs=200, batch_size=50), fallback])
	data = agent.load_data(resource_name=training_data_file, augmentation_factor=0)	
	

	agent.train(data)
				
	agent.persist(model_path)
	return agent
	
	
train_dialogue()